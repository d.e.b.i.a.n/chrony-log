#! /bin/bash

LOG_DIR=/run/chrony-log
ARCH_DIR=/var/log/chrony

# report some statistics about the files being flushed
/usr/bin/wc --lines --bytes $LOG_DIR/*.log

### using for f in $(find...) to avoid getting glob as result if there's no match

#for f in $LOG_DIR/*.log
for f in $(find "$LOG_DIR" -name '*.log')
do
	/usr/bin/mv "$f" "$f.old"
done

# this is expected to complain when it's run from ExecStopPost
/usr/bin/chronyc cyclelogs

#for f in $LOG_DIR/*.log.old
for f in $(find $LOG_DIR -name '*.log.old')
do
	t=${f%%.old}
	target=$(/usr/bin/basename "$t")
	/usr/bin/cat "$f" >>"$ARCH_DIR"/"$target"
	/usr/bin/rm "$f"
done
