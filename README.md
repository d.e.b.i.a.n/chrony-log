Buffering chrony's log files
============================

Chrony appears to do line buffering, which results in less than disk block
write for each line of output. On flash storage, most of these will (absent
filesystem write deferment) require a wasteful read, modify, write. 
chrony-log inserts some buffering by having chrony write to files in
/run/chrony-log (/run being a tmpfs filesystem), and periodically flushing
the accumulated logs to disk (flash) in the usual place (/var/log/chrony).

Some numbers from a 20 minute flush cycle, with a local PPS and three NTP
sources (wc --lines --bytes /run/chrony-log/*.log):
```
   63  8631 /run/chrony-log/measurements.log
  147 17199 /run/chrony-log/statistics.log
   84 11088 /run/chrony-log/tracking.log
  294 36918 total
```

Installation
------------

The repo mirrors my local setup in Debian 11 & 12. bin -> /usr/local/bin,
systemd/... -> /usr/local/systemd/...

There is one change to the chrony config: set logdir to /run/chrony-log.
NB: this can't be done with a conf.d drop-in in the current stock Debian
setup, you have to edit chrony.conf.

Enable chrony-log, stop chrony, daemon-reload, start chrony.  Might need to
start chrony-log when doing this by hand, but the units work together
properly at boot.  Oh, and restart logrotate to ensure that unlikely race
can't get triggered.

If not on Debian some of this may need to be adjusted.
